import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadLocadorRoutingModule } from './cad-locador-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadLocadorRoutingModule
  ]
})
export class CadLocadorModule { }
